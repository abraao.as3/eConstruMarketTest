eConstruMarketTest
==================

> ### Requisitos
> Para executar o sistema é necessário ter o Node JS v8+ e NPM v5.4+ instalado

- Intalar as dependências:

```
npm i
```

- executar o sistema:

```
npm run dev
```
