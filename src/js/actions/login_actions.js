import isEmail from 'validator/lib/isEmail';
import axios from 'axios';

import Store from '../Store';

export function login(email, pass){
    var sizePass = 4;
    var login_url = "https://swapi.co/api/people/1/";

    if(!!!email.value || !isEmail(email.value)){
        email.value = "";
        pass.value = "";
        return {
            type: "NO_EMAIL",
            payload: email
        };
    }

    if(!!!pass.value){
        pass.value = "";
        return {
            type: "NO_PASS",
            payload: pass
        };
    }

    if(pass.value.length < sizePass){
        pass.value = "";
        return {
            type: "SHORT_PASS",
            payload: pass
        }
    }

    return {
        type: "SEND_LOGIN",
        payload: axios.get(login_url)
        .then(res => {
            Store.store.dispatch({type: 'CONNECTED', payload: res.data});
            Store.history.push('/produtos');
            return {
                type: 'SUCCESS',
                payload: res.data
            }
        })
    }
}
