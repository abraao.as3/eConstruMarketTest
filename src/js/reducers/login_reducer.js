const login_reducer = (state = {
    blocked: false
}, action) => {
    switch (action.type) {
        case 'NO_EMAIL':
            state = {
                ...state,
                blocked: false,
                message: 'E-mail inválido'
            }
            action.payload.focus();
            break;
        case 'NO_PASS':
            state = {
                ...state,
                message: 'Senha inválida',
                blocked: false
            }
            action.payload.focus();
            break;
        case 'SHORT_PASS':
            state = {
                ...state,
                message: 'Senha muito curta',
                blocked: false
            }
            action.payload.focus();
            break;
        case 'SEND_LOGIN':
            state = {
                ...state,
                blocked: true,
                message: null
            }
            break;
    }

    return state;
}

export default login_reducer;
