const user_reducer = (state = {
    connected: false
}, action) => {
    switch (action.type) {
        case 'DISCONNECTED':
            state = {
                connected: false
            }
            break;
        case 'CONNECTED':
            state = {
                ...state,
                ...action.payload,
                connected: true
            }

            console.log(state);
            break;
    }

    return state;
}

export default user_reducer;
