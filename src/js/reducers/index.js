import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux';

import login_reducer from './login_reducer';
import user_reducer from './user_reducer';

export default combineReducers({
    router: routerReducer,
    login: login_reducer,
    user: user_reducer
});
