import React from 'react';
import ReactDom from 'react-dom';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { Provider } from 'react-redux';

import css from '../sass/style.scss';

import Home from './telas/Home';

import Store from './Store';


window.onload = ()=>{
    var root = document.createElement('div');
    document.body.appendChild(root);

    ReactDom.render(
        <Provider store={Store.store}>
            <ConnectedRouter history={Store.history}>
                <Route path='/' component={Home} />
            </ConnectedRouter>
        </Provider>,
        root
    );
}
