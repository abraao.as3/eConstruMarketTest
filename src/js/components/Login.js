import autobind from 'autobind-decorator';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { login } from '../actions/login_actions';

import {
    BackgroundImage, Flex, Fixed,
    Overlay, Container, Input,
    Box, Divider, Button, Link,
    Subhead
} from 'rebass';

import Colors from '../utils/Colors';
import background from '../../images/construcao_back.jpg';
import FaUserO from 'react-icons/lib/fa/user';
import FaEnvelopeO from 'react-icons/lib/fa/envelope-o';

@autobind
class Login extends React.Component{
    @Private _email = null;
    @Private _pass = null;

    componentDidMount(){
        this._email = document.getElementById('login_page_mail');
        this._pass = document.getElementById('login_page_pass');
    }

    @Private
    inputEvent(e){
        if(e.key == 'Enter'){
            this.sendLogin();
        }
    }

    @Private
    sendLogin(){
        this.props.dispatch({type: "SEND_LOGIN"});
        this.props.dispatch(login(this._email, this._pass));
    }

    render(){
        return (
            <Flex w={1}>
                <Fixed top right left bottom>
                    <BackgroundImage src={background} ratio={1/3} style={{height: "100%"}} />
                </Fixed>
                <Fixed top right left bottom bg={"rgba(0, 0.7)"} />
                <Overlay w={[0.8, 0.6, 0.6, 0.5]} bg="rgba(0,0,0,0.5)" p={0} className="login_box">
                    <Container bg="rgba(255,255,255,0.3)" p={20} className="login_box">
                        <Flex>
                            <Subhead f={3} mt={2} mb={2} color={Colors.BLACK} ml="auto" mr="auto">
                                {this.props.login.message}
                            </Subhead>
                        </Flex>
                        <Flex>
                            <Box mt="auto" mb="auto" w={25}>
                                <FaUserO color="white" width={20} height={20} />
                            </Box>
                            <Input
                                id="login_page_mail"
                                placeholder="e-mail"
                                color="white"
                                width={1}
                                className="login_form"
                                onKeyPress={this.inputEvent}
                                disabled={this.props.login.blocked}
                            />
                        </Flex>
                        <Divider
                        	w={1}
                        	color='white'
                        />
                        <Flex>
                            <Box mt="auto" mb="auto" w={25}>
                                <FaEnvelopeO color="white" width={20} height={20} />
                            </Box>
                            <Input
                                id="login_page_pass"
                                placeholder="senha"
                                color="white"
                                width={1}
                                type="password"
                                className="login_form"
                                onKeyPress={this.inputEvent}
                                disabled={this.props.login.blocked}
                            />
                        </Flex>
                        <Divider
                        	w={1}
                        	color='white'
                        />
                        <Flex wrap>
                            <Link
                                w={[1, 1/2]}
                                mt={[2, 0]}
                                color="white"
                                children="Esqueci a senha"
                                href="#"
                            />
                            <Link
                                w={[1, 1/2]}
                                mt={[2, 0]}
                                color="white"
                                children="Cadastrar-se"
                                href="#"
                            />
                        </Flex>
                        <Flex>
                            <Button
                                children="entrar"
                                f={2}
                                bg={Colors.ORANGE}
                                width={[1/2, 1/2, 1/4]}
                                mt={4}
                                ml="auto"
                                mr="auto"
                                className="topo_form topo_button login_button"
                                onClick={this.sendLogin}
                                disabled={this.props.login.blocked}
                             />
                        </Flex>
                    </Container>
                </Overlay>
            </Flex>
        );
    }
}

const mapStateToProps = state => {
    return {
        login: state.login
    }
}

export default withRouter(connect(mapStateToProps)(Login));
