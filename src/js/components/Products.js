import autobind from 'autobind-decorator';
import React from 'react';

import {
    Flex, Box
} from 'rebass';

import Menu from './menu/Menu';

@autobind
class Products extends React.Component{
    render(){
        return (
            <Flex w={1}>
                <Menu />
                <Box>
                    Teste
                </Box>
            </Flex>
        );
    }
}

export default Products;
