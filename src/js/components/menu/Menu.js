import autobind from 'autobind-decorator';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {
    Box, Fixed, Container, Flex, Lead,
    Text, NavLink
} from 'rebass';

import FaUserO from 'react-icons/lib/fa/user';
import FaHome from 'react-icons/lib/fa/home';
import FaShoppingCart from 'react-icons/lib/fa/shopping-cart';
import FaStar from 'react-icons/lib/fa/star';
import FaEnvelope from 'react-icons/lib/fa/envelope';

import Colors from '../../utils/Colors';

@autobind
class Menu extends React.Component{
    render(){
        return(
            <Box w={0.15}>
                <Fixed top left bottom bg={Colors.BLACK} w={0.15}  color={Colors.GREEN}>
                    <Container>
                        <Flex w={1} mt={50}>
                            <Lead bg={Colors.GREEN} ml="auto" mr="auto" className="menu_user_circle">
                                <FaUserO width={100} height={100} color={Colors.BLACK} />
                            </Lead>
                        </Flex>

                        <Text
                            mt={2}
                            center
                            color="white"
                            children={this.props.user.name}
                        />

                        <Flex w={1} mt={20} wrap>
                            <NavLink w={1}>
                                <FaHome width={20} height={20}/>
                                <Text
                                    ml={5}
                                    f={18}
                                    children="Home"
                                />
                            </NavLink>
                            <NavLink w={1}>
                                <FaStar width={20} height={20}/>
                                <Text
                                    ml={5}
                                    f={18}
                                    children="Sobre Nós"
                                />
                            </NavLink>
                            <NavLink w={1}>
                                <FaShoppingCart width={20} height={20}/>
                                <Text
                                    ml={5}
                                    f={18}
                                    children="Produtos"
                                />
                            </NavLink>
                            <NavLink w={1}>
                                <FaEnvelope width={20} height={20}/>
                                <Text
                                    ml={5}
                                    f={18}
                                    children="Contato"
                                />
                            </NavLink>
                        </Flex>
                    </Container>
                </Fixed>
            </Box>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.user
    }
}

export default withRouter(connect(mapStateToProps)(Menu));
