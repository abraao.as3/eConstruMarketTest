class Colors{
    static ORANGE = "#fb5b09";
    static BLUE = "#4799bf";
    static GREY = "#cbcdcc";
    static GREEN = "#48c070";
    static BLACK = "#4c4d4c";

}

export default Colors;
