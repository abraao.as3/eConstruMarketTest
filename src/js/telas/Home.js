import React from 'react';
import autobind from 'autobind-decorator';
import { Switch, Route } from 'react-router-dom';
import { Flex } from 'rebass';

import Login from '../components/Login';
import Products from '../components/Products';


@autobind
class Home extends React.Component{
    render(){
        return (
            <Flex wrap>
                <Switch>
                    <Route exact path="/" component={Login} />
                    <Route path="/produtos" component={Products} />
                </Switch>
            </Flex>
        );
    }
}

export default Home;
